package com.typinspace.myapplication.app;

/**
 * Created by Pikiring Waskitha on 13/11/2015.
 */
public class AppConfig {

    public static String IP_ADDRESS="http://192.168.24.124";

    // Server user login url
    public static String URL_LOGIN = IP_ADDRESS+"/GePresence/response/login.php";

    // Server user register url
    public static String URL_REGISTER = IP_ADDRESS+"/GePresence/response/register.php";

    // Server QRCode url
    public static String URL_QRCODE = IP_ADDRESS+"/GePresence/response/checkQRcode.php";

    // Server IZIN url
    public static String URL_IZIN = IP_ADDRESS+"/GePresence/response/requestizin.php";

}
