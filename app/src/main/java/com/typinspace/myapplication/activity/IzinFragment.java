package com.typinspace.myapplication.activity;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.typinspace.myapplication.R;
import com.typinspace.myapplication.helper.DateTime;
import com.typinspace.myapplication.helper.DateTimePicker;
import com.typinspace.myapplication.helper.SimpleDateTimePicker;

import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class IzinFragment extends Fragment implements DateTimePicker.OnDateTimeSetListener {

    HomeActivity home;

    TextView content;
    TextView content2;
    EditText ket;

    String pergi,kembali,keterangan;
    int p=0,k=0;

    public IzinFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_izin, container, false);
        View click = v.findViewById(R.id.ClickMeIfYouCan);
        View click2 = v.findViewById(R.id.ClickMeIfYouCan2);
        View kirim = v.findViewById(R.id.kirim_coy);
        content = (TextView) v.findViewById(R.id.content);
        content2 = (TextView) v.findViewById(R.id.content2);
        ket = (EditText) v.findViewById(R.id.keterangan);

        //Get or Generate Date
        Date todayDate = new Date();
        DateTime mDateTime = new DateTime(todayDate);

        if (mDateTime.getDateString() != null){
            content.setText(mDateTime.getDateString());
            content2.setText(mDateTime.getDateString());
            pergi = mDateTime.getDateString();
            kembali = mDateTime.getDateString();
        }

        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateTimePicker();
                p++;
            }
        });

        click2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateTimePicker();
                k++;
            }
        });

        //Konstruktor
        home = (HomeActivity) getActivity();

        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertKirim();
            }
        });

        return v;
    }

    @Override
    public void DateTimeSet(Date date) {

        // This is the DateTime class we created earlier to handle the conversion
        // of Date to String Format of Date String Format to Date object
        DateTime mDateTime = new DateTime(date);
        // Show in the LOGCAT the selected Date and Time
        if (mDateTime.getDateString() != null){
            if (p>=1){
                //parsing date ke dalam variabel
                pergi = mDateTime.getDateString();
                kembali = mDateTime.getDateString();

                //rubah display di layar
                content.setText(pergi);
                content2.setText(pergi);

                p=0;
            } else if (k>=1){

                kembali = mDateTime.getDateString();
                content2.setText(kembali);

                k=0;
            } else {

                p=0; k=0;

            }

        }

        Log.v("TEST_TAG", "Date and Time selected: " + mDateTime.getDateString());
    }

    public void showDateTimePicker(){
        // Or we can chain it to simplify
        SimpleDateTimePicker.make(
                "Permintaan izin",
                new Date(),
                this,
                getFragmentManager()
        ).show();
    }

    public void showAlertKirim(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());

        // set title
        alertDialogBuilder.setTitle("Izin jam kerja");

        // set dialog message
        alertDialogBuilder
                .setMessage("Klik ya untuk mengirim!")
                .setCancelable(false)
                .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                        dialog.dismiss();

                        keterangan=ket.getText().toString();
                        if (keterangan.isEmpty()){
                            keterangan="Tidak ada alasan!";
                        }

                        home.submitIzin(pergi,kembali,keterangan);

                    }
                })
                .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

}
