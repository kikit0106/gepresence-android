package com.typinspace.myapplication.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.typinspace.myapplication.R;
import com.typinspace.myapplication.app.AppConfig;
import com.typinspace.myapplication.app.AppController;
import com.typinspace.myapplication.helper.SQLiteHandler;
import com.typinspace.myapplication.helper.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BackupActivity extends AppCompatActivity {

    private static final String TAG = BackupActivity.class.getSimpleName();
    private TextView txtNama;
    private TextView txtEmail;
    private TextView txtMac;
    private Button btnLogout;
    private Button datang;
    private Button pulang;

    public String condition = "null";

    private SQLiteHandler db;
    private SessionManager session;
    private ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backup);

        txtNama = (TextView)findViewById(R.id.nama3);
        txtEmail= (TextView)findViewById(R.id.email3);
        txtMac  = (TextView)findViewById(R.id.mac_address3);
        btnLogout=(Button)findViewById(R.id.btnLogout);
        datang = (Button)findViewById(R.id.btnIn);
        pulang = (Button)findViewById(R.id.btnOut);

        //SQLite handler
        db= new SQLiteHandler(getApplicationContext());

        //Session manager
        session = new SessionManager(getApplicationContext());

        //progress Dialog
        pDialog=new ProgressDialog(this);
        pDialog.setCancelable(false);

        if (!session.isLoggedIn()){
            logoutUser();
        }

        //Fetching user details from SQLite
        HashMap<String,String > user = db.getUserDetails();

        String nama= user.get("nama");
        String email = user.get("email");
        String mac = user.get("mac");


        //Displaying user details
        txtNama.setText(nama);
        txtEmail.setText(email);
        txtMac.setText(mac);

        //button datang event
        datang.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                condition = "datang";
                openScanner(condition);
            }
        });

        //button pulang event
        pulang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                condition = "pulang";
                openScanner(condition);
            }
        });

        //Logout button event
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });
    }

    // get id_karyawan from SQLite Database
    public String getIdKaryawan(){
        HashMap<String,String > user = db.getUserDetails();
        String id=user.get("id_karyawan");
        return id;
    }

    //open QRCode scanner
    public void openScanner(final String condition){
        Toast toast = Toast.makeText(getApplicationContext(),
                "camera open!", Toast.LENGTH_SHORT);
        toast.show();

        IntentIntegrator integrator = new IntentIntegrator(BackupActivity.this);
        integrator.setCaptureActivity(CaptureActivityAnyOrientation.class);
        integrator.setOrientationLocked(false);
        integrator.setCameraId(0);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.addExtra("Condition", condition);
        integrator.addExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
        //customize the prompt message before scanning
        integrator.setPrompt("Scan QRCode from server");
        integrator.initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (result != null) {
            String contents = result.getContents();
                if (contents != null) {
                    checkQRCodeContent(contents,condition);
                } else {
                    Toast toast =Toast.makeText(getApplicationContext(),
                            "Scan cancelled!", Toast.LENGTH_SHORT);
                    toast.show();
                }
        }  else {
            Toast toast =Toast.makeText(getApplicationContext(),
                    "Scan failed!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * check today QRCode from server
     */
    public void checkQRCodeContent(final String content,final String condition){
        //Tag used to cancel request
        String tag_string_req="req_qrcode";

        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_QRCODE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Check Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // do something (input jam datang or pulang)

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Check QRCode Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to QRCode url
                Map<String, String> params = new HashMap<>();
                params.put("result", content);
                params.put("condition",condition);
                params.put("id_karyawan",getIdKaryawan());

                return params;
            }

        };

        // Adding request to request queue
        AppController.getmInstance().addToRequestQueue(strReq, tag_string_req);
        }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    public void logoutUser(){
        session.setLogin(false);

        db.deleteUser();

        //Go to login activity
        Intent intent =new Intent(getApplicationContext(),LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
