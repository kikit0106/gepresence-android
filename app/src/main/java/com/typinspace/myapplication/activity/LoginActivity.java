package com.typinspace.myapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.typinspace.myapplication.R;
import com.typinspace.myapplication.app.AppConfig;
import com.typinspace.myapplication.app.AppController;
import com.typinspace.myapplication.helper.SQLiteHandler;
import com.typinspace.myapplication.helper.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;



public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private CoordinatorLayout coordinatorLayout;
    private Button btnLogin;
    private Button btnRegisterLink;
    private EditText inputEmail;
    private EditText inputPassword;
    private TextView inputMac;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        coordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinatorLayout1);
        inputEmail = (EditText)findViewById(R.id.email);
        inputPassword = (EditText)findViewById(R.id.password);
        inputMac = (TextView)findViewById(R.id.mac_address);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnRegisterLink = (Button)findViewById(R.id.btnLinkToRegisterScreen);

        //show device mac address
        inputMac.setText(getMAC());

        //progress Dialog
        pDialog=new ProgressDialog(this);
        pDialog.setCancelable(false);

        //SQLite database
        db=new SQLiteHandler(getApplicationContext());

        //Session Manager
        session=new SessionManager(getApplicationContext());

        //check logged in user
        if (session.isLoggedIn()){
            //if logged in,take to main activity
            Intent intent=new Intent(LoginActivity.this,BackupActivity.class);
            startActivity(intent);
            finish();
        }

        //Login button click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email= inputEmail.getText().toString();
                String password= inputPassword.getText().toString();
                String mac= getMAC();

                //check for empty data input
                if (!email.isEmpty()&&!password.isEmpty()){
                    //login user
                    if (mac!=null){
                        checkLogin(email,password,mac);
                    } else {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout,
                                R.string.alert_mac,Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }

                } else {
                    //prompt user to enter credentials
                    Snackbar snackbar = Snackbar.make(coordinatorLayout,
                            R.string.alert_login,Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });

        //Link to register activity
        btnRegisterLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    /**
     * function to verify login detail in mysql db
     */

    private void checkLogin(final String email, final String password,final String mac) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        StringRequest strReq = new StringRequest(Method.POST,
                AppConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // Now store the user in SQLite
                        JSONObject user = jObj.getJSONObject("user");
                        int id = user.getInt("id_karyawan");
                        String nama = user.getString("nama");
                        String mac = user.getString("mac");
                        String telepon = user.getString("telepon");
                        String email = user.getString("email");
                        String created_at = user.getString("created_at");

                        // Inserting row in users table
                        db.addUser(id,nama,mac,telepon,email,created_at);

                        // Launch main activity
                        Intent intent = new Intent(LoginActivity.this,
                                HomeActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Tidak terhubung dengan server!", Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);
                params.put("mac",mac);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getmInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public String getMAC(){
        final WifiManager wm = (WifiManager) getBaseContext().getSystemService(Context
                .WIFI_SERVICE);
        final WifiInfo info = wm.getConnectionInfo();
        String mac_add = info.getMacAddress();

        return mac_add;
    }
}
