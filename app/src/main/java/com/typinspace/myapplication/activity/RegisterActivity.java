package com.typinspace.myapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.typinspace.myapplication.R;
import com.typinspace.myapplication.app.AppConfig;
import com.typinspace.myapplication.app.AppController;
import com.typinspace.myapplication.helper.SQLiteHandler;
import com.typinspace.myapplication.helper.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = RegisterActivity.class.getCanonicalName();
    private CoordinatorLayout coordinatorLayout;
    private Button btnRegister;
    private Button btnLoginLink;
    private EditText inputNama;
    private EditText inputEmail;
    private EditText inputPassword;
    private EditText inputTelepon;
    private TextView inputMac;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        coordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinatorLayout2);
        btnRegister     =(Button)findViewById(R.id.btnRegister);
        btnLoginLink    =(Button)findViewById(R.id.btnLinkToLoginScreen);
        inputNama   =(EditText)findViewById(R.id.nama2);
        inputEmail  =(EditText)findViewById(R.id.email2);
        inputPassword =(EditText)findViewById(R.id.password2);
        inputTelepon  =(EditText)findViewById(R.id.telepon2);
        inputMac    =(TextView)findViewById(R.id.mac_address2);

        //progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        //show mac address
        inputMac.setText(getMAC());

        //Session Manager
        session = new SessionManager(getApplicationContext());

        //SQLite database
        db = new SQLiteHandler(getApplicationContext());

        //Check if user already logged in
        if (session.isLoggedIn()){
            Intent intent = new Intent(RegisterActivity.this,BackupActivity.class
            );
            startActivity(intent);
            finish();
        }

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama = inputNama.getText().toString();
                String email= inputEmail.getText().toString();
                String password=inputPassword.getText().toString();
                String telepon=inputTelepon.getText().toString();
                String mac=getMAC();

                //check for empty data input
                if (!email.isEmpty()&&!password.isEmpty()){
                    //login user
                    if (mac!=null){
                        registerUser(nama, email, password, telepon, mac);
                    } else {
                        Snackbar snackbar = Snackbar.make(coordinatorLayout,
                                R.string.alert_mac,Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }

                } else {
                    //prompt user to enter credentials
                    Snackbar snackbar = Snackbar.make(coordinatorLayout,
                            R.string.alert_register,Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });

        btnLoginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    /**
     * Function to store user in MySQL database will post params(tag, name,
     * email, password, telephone and mac address) to register url
     * */
    public void registerUser(final String nama,final String email,final String password,final String
            telepon,final String mac){
        // Tag used to cancel the request
        String tag_string_req = "req_register";

        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        StringRequest strReq = new StringRequest(Method.POST,
                AppConfig.URL_REGISTER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Register Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        // User successfully stored in MySQL
                        // Now store the user in sqlite
//                        String uid = jObj.getString("uid");

                        // Now store the user in SQLite
                        JSONObject user = jObj.getJSONObject("user");
                        int id = user.getInt("id_karyawan");
                        String nama = user.getString("nama");
                        String mac = user.getString("mac");
                        String telepon = user.getString("telepon");
                        String email = user.getString("email");
                        String created_at = user.getString("created_at");

                        // Inserting row in users table
                        db.addUser(id,nama,mac,telepon,email,created_at);

                        Toast.makeText(getApplicationContext(),"Registrasi berhasil!", Toast
                                .LENGTH_LONG)
                                .show();
                        Log.d(TAG, "Berhasil!");

                        // Launch login activity
                        Intent intent = new Intent(
                                RegisterActivity.this,
                                LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {

                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Tidak terhubung dengan server!", Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("nama", nama);
                params.put("email", email);
                params.put("password", password);
                params.put("telepon",telepon);
                params.put("mac",mac);

                return params;
            }

        };

        // Adding request to requeste queue
        AppController.getmInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public String getMAC(){
        final WifiManager wm = (WifiManager) getBaseContext().getSystemService(Context
                .WIFI_SERVICE);
        final WifiInfo info = wm.getConnectionInfo();
        String mac= info.getMacAddress();
        return mac;
    }
}
