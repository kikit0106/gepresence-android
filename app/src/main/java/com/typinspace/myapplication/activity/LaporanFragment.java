package com.typinspace.myapplication.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.typinspace.myapplication.R;
import com.typinspace.myapplication.app.AppConfig;

import java.text.DateFormatSymbols;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class LaporanFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    public LaporanFragment() {
        // Required empty public constructor
    }

    HomeActivity home;
    private static final String TAG = LaporanFragment.class.getCanonicalName();
    private SwipeRefreshLayout swipeRefreshLayout;
    private WebView webView;
    private String url,id_karyawan;
    private int tahun,bulan;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_laporan, container, false);

        //My code begin
        home = (HomeActivity) getActivity();
        this.id_karyawan=home.id_karyawan;

        Calendar now = Calendar.getInstance();
        tahun = now.get(Calendar.YEAR);
        bulan = now.get(Calendar.MONTH)+1;

        swipeRefreshLayout  = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        webView = (WebView) v.findViewById(R.id.webview);
        webView.setWebViewClient(new MyBrowser());

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,R.color.green);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                loadLaporan();
            }
        });
        return v;
    }

    @Override
    public void onRefresh() {
        loadLaporan();
    }

    private void loadLaporan(){
        url= AppConfig.IP_ADDRESS+"/gepresence/lap-webview.php?id="+id_karyawan+"&bulan="+bulan+"&tahun="+tahun;
        Log.d(TAG,url);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadUrl(url);
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url){
            view.loadUrl(url);
            return true;
        }

        // when finish loading page
        public void onPageFinished(WebView view, String url) {
            swipeRefreshLayout.setRefreshing(false);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String
                failingUrl) {
            view.loadUrl("file:///android_asset/loadError.html");
        }
    }

    private String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11 ) {
            month = months[num];
        }
        return month;
    }

}
