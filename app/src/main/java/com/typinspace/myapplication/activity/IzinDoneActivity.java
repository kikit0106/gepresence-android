package com.typinspace.myapplication.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.typinspace.myapplication.R;

public class IzinDoneActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_izin_done);

        View kembali = findViewById(R.id.kembali);

        TextView jp = (TextView) findViewById(R.id.jp);
        TextView jk = (TextView) findViewById(R.id.jk);
        TextView kp = (TextView) findViewById(R.id.kp);

        Bundle b =getIntent().getExtras();
        jp.setText(b.getString("pergi"));
        jk.setText(b.getString("kembali"));
        kp.setText(b.getString("keperluan"));

        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }
}
