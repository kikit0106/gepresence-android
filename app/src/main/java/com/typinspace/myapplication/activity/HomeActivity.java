package com.typinspace.myapplication.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.typinspace.myapplication.R;
import com.typinspace.myapplication.app.AppConfig;
import com.typinspace.myapplication.app.AppController;
import com.typinspace.myapplication.helper.SQLiteHandler;
import com.typinspace.myapplication.helper.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = BackupActivity.class.getSimpleName();
    private TextView headerNama;
    private TextView headerEmail;

    public String condition = "null";
    public String id_karyawan;

    private SQLiteHandler db;
    private SessionManager session;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_home);

        /**
         * copy paste from old BackupActivity
         */
        headerNama = (TextView)headerLayout.findViewById(R.id.header_nama);
        headerEmail = (TextView)headerLayout.findViewById(R.id.header_email);

        //SQLite handler
        db= new SQLiteHandler(getApplicationContext());

        //Session manager
        session = new SessionManager(getApplicationContext());

        //progress Dialog
        pDialog=new ProgressDialog(this);
        pDialog.setCancelable(false);

        if (!session.isLoggedIn()){
            logoutUser();
        }

        //Fetching user details from SQLite
        HashMap<String,String > user = db.getUserDetails();

        String nama= user.get("nama");
        String email = user.get("email");
        id_karyawan = user.get("id_karyawan");

        headerNama.setText(nama);
        headerEmail.setText(email);

        if (savedInstanceState==null){
            setFragment(PresensiFragment.class);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this,R.style.AppCompatAlertDialogStyle);
            alert.setMessage("Anda yakin untuk keluar??");
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    logoutUser();
                }
            });
            alert.setNegativeButton("Batal", null);
            alert.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_presensi) {
            setFragment(PresensiFragment.class);
        }else if (id == R.id.nav_izin) {
            setFragment(IzinFragment.class);
        }else if (id == R.id.nav_laporan) {
            setFragment(LaporanFragment.class);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //method to show fragment on activity
    public void setFragment( Class<? extends Fragment> fragmentClass) {
        try {
            Fragment fragment = fragmentClass.newInstance();
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame, fragment, fragmentClass.getSimpleName());
            fragmentTransaction.commit();
        }
        catch (Exception ex){
            Log.e("setFragment", ex.getMessage());
        }
    }

    //Result show
    public void showResult(String f,String c){
        LayoutInflater mInflater = LayoutInflater.from(this);
        View v = mInflater.inflate(R.layout.result,null);

        final AlertDialog dialog = new AlertDialog.Builder(this).create();

        dialog.setView(v);
        dialog.setTitle(""+f);


        //Handle TextLayout google i/o 2015 dan input Username
        final TextView content = (TextView) v.findViewById(R.id.content);
        final Button btnOK    = (Button) v.findViewById(R.id.ok);

        content.setText(""+c);

        dialog.show();

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    /**
     * copy paste from old BackupActivity
     */
    // get id_karyawan from SQLite Database
    public String getIdKaryawan(){
        HashMap<String,String > user = db.getUserDetails();
        String id=user.get("id_karyawan");
        return id;
    }

    //open QRCode scanner
//    public void openScanner(){
//        Toast toast = Toast.makeText(getApplicationContext(),
//                "camera open!", Toast.LENGTH_SHORT);
//        toast.show();
//
//        IntentIntegrator integrator = new IntentIntegrator(HomeActivity.this);
//        integrator.setCaptureActivity(CaptureActivityAnyOrientation.class);
//        integrator.setOrientationLocked(false);
//        integrator.setCameraId(0);
//        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
//        integrator.addExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
//        //customize the prompt message before scanning
//        integrator.setPrompt("Scan QRCode from server");
//        integrator.initiateScan();
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (result != null) {
            String contents = result.getContents();
            if (contents != null) {
                checkQRCodeContent(contents,condition);
            } else {
                Toast toast =Toast.makeText(getApplicationContext(),
                        "Scan cancelled!", Toast.LENGTH_SHORT);
                toast.show();
            }
        }  else {
            Toast toast =Toast.makeText(getApplicationContext(),
                    "Scan failed!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * check today QRCode from server
     */
    public void checkQRCodeContent(final String content,final String condition){
        //Tag used to cancel request
        String tag_string_req="req_qrcode";

        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_QRCODE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Check Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // do something (input jam datang or pulang)
                        showResult("QRCode Result","Input jam "+condition+" berhasil!");
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Check QRCode Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Tidak terhubung dengan server!", Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to QRCode url
                Map<String, String> params = new HashMap<>();
                params.put("result", content);
                params.put("condition",condition);
                params.put("id_karyawan",getIdKaryawan());

                return params;
            }

        };

        // Adding request to request queue
        AppController.getmInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    /**
     * Submit izin request to server
     */
    public void submitIzin(final String pergi,final String kembali,final String keperluan){
        //Tag used to cancel request
        String tag_string_req="req_izin";

        pDialog.setMessage("Mohon tunggu...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_IZIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Check Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // do something (input jam datang or pulang)
                        Bundle bundle = new Bundle();
                        bundle.putString("pergi",pergi);
                        bundle.putString("kembali", kembali);
                        bundle.putString("keperluan", keperluan);


                        Intent i=new Intent(HomeActivity.this,IzinDoneActivity.class);
                        i.putExtras(bundle);
                        startActivity(i);

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Check IZIN Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Tidak terhubung dengan server!", Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to QRCode url
                Map<String, String> params = new HashMap<>();
                params.put("pergi",pergi);
                params.put("kembali",kembali);
                params.put("keperluan",keperluan);
                params.put("id_karyawan",getIdKaryawan());

                return params;
            }

        };

        // Adding request to request queue
        AppController.getmInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    public void logoutUser(){
        session.setLogin(false);
        db.deleteUser();

        //Go to login activity
        Intent intent =new Intent(getApplicationContext(),LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String c){
        this.condition = c;
    }

}
