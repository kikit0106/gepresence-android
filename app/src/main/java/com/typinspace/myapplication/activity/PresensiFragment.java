package com.typinspace.myapplication.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.typinspace.myapplication.R;
import com.typinspace.myapplication.helper.SQLiteHandler;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class PresensiFragment extends Fragment {

    private TextView txtNama;
    private TextView txtEmail;
    private TextView txtMac;
    private Button datang;
    private Button pulang;

    private SQLiteHandler db;

    HomeActivity ha;

    public PresensiFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_presensi, container, false);

        //Konstruktor
        ha = (HomeActivity) getActivity();

        txtNama = (TextView)v.findViewById(R.id.nama3);
        txtEmail= (TextView)v.findViewById(R.id.email3);
        txtMac  = (TextView)v.findViewById(R.id.mac_address3);
        datang = (Button)v.findViewById(R.id.btnIn);
        pulang = (Button)v.findViewById(R.id.btnOut);

        //SQLite handler
        db= new SQLiteHandler(getContext());

        //Fetching user details from SQLite
        HashMap<String,String > user = db.getUserDetails();

        String nama= user.get("nama");
        String email = user.get("email");
        String mac = user.get("mac");


        //Displaying user details
        txtNama.setText(nama);
        txtEmail.setText(email);
        txtMac.setText(mac);

        //button datang event
        datang.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ha.setCondition("datang");
                openScanner();
            }
        });

        //button pulang event
        pulang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ha.setCondition("pulang");
                openScanner();
            }
        });

        return v;
    }

    //open QRCode scanner
    public void openScanner(){
        Toast toast = Toast.makeText(getContext(),
                "camera open!", Toast.LENGTH_SHORT);
        toast.show();

        IntentIntegrator integrator = new IntentIntegrator(getActivity());
        integrator.setCaptureActivity(CaptureActivityAnyOrientation.class);
        integrator.setOrientationLocked(false);
        integrator.setCameraId(0);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.addExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
        //customize the prompt message before scanning
        integrator.setPrompt("Scan QRCode from server");
        integrator.initiateScan();
    }

}
