package com.typinspace.myapplication.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by Pikiring Waskitha on 20/11/2015.
 */
public class SQLiteHandler extends SQLiteOpenHelper {
    private static final String TAG = SQLiteHandler.class.getSimpleName();

//All Static variable
    private static final int DATABASE_VERSION =1;
    private static final String DATABASE_NAME = "geschool";
    private static final String TABLE_USER = "karyawan";

    //table column name
    private static final String KEY_ID = "id_karyawan";
    private static final String KEY_NAMA = "nama";
    private static final String KEY_MAC = "mac";
    private static final String KEY_TELEPON = "telepon";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_CREATED_AT = "created_at";

    public SQLiteHandler (Context context){
        super(context, DATABASE_NAME,null,DATABASE_VERSION);
    }

    //Creating Table
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOGIN_TABLE = "CREATE TABLE IF NOT EXISTS "+TABLE_USER+"("
                +KEY_ID+" INTEGER PRIMARY KEY,"+KEY_NAMA+" TEXT,"
                +KEY_MAC+" TEXT UNIQUE,"+KEY_TELEPON+" TEXT,"
                +KEY_EMAIL+" TEXT UNIQUE,"+KEY_CREATED_AT+" TEXT"
                +")";
        db.execSQL(CREATE_LOGIN_TABLE);

        Log.d(TAG, "Database table created!");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_USER);

        //crete table again
        onCreate(db);
    }
    /**
     * Storing karyawan details in SQLite database
     */
    public void addUser(int id, String nama,String mac,String telepon,String email,String
            created_at){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values=new ContentValues();
        values.put(KEY_ID,id);
        values.put(KEY_NAMA,nama);
        values.put(KEY_MAC,mac);
        values.put(KEY_TELEPON,telepon);
        values.put(KEY_EMAIL,email);
        values.put(KEY_CREATED_AT,created_at);

        //insert Row
        db.insert(TABLE_USER,null,values);
        db.close();

        Log.d(TAG, "New user on SQLite: " + id);
    }

    /*
    Getting karyawan data from SQLite database
     */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<>();
        String selectQuery = "SELECT * FROM "+TABLE_USER;

        SQLiteDatabase db= this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        //Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0){
            user.put("id_karyawan",cursor.getString(0));
            user.put("nama",cursor.getString(1));
            user.put("mac",cursor.getString(2));
            user.put("telepon",cursor.getString(3));
            user.put("email",cursor.getString(4));
            user.put("created_at",cursor.getString(5));
        }
        cursor.close();
        db.close();
        //return karyawan
        Log.d(TAG,"Fetching user from SQLite: " + user.toString());

        return user;
    }
    /*
     Re crate database Delete all tables and create them again
     */
    public void deleteUser(){
        SQLiteDatabase db = this.getReadableDatabase();
        //Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.close();

        Log.d(TAG,"Deleted all karyawan info from SQLite!");
    }
}
